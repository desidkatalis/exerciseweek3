# Profile Task Tracker

    Create REST API with HapiJS and MongoDB

## Table of contents

* What is the approach?
* Why is the approach followed?
* Problem faced while using the approach
* Step to run code

## The approach that i used

* First : Create method GET, POST, PUT, DELETE
* Second : Create testing with lab => test if all the method path is run well
* Third : Create basic authentication => manage users

## The reason why i use that way to create my project

    Because its a simple way to make the Profile Task Tracker

## Problem that frequently appear when i use that way

* Error whin installing @hapi/joi, it should use the version @15.1.1
* Testing

## Step to run code

* First : Make sure you are already install package

    * @hapi/joi => 15.1.1
    * @hapi/hapi
    * qs
    * bcrypt
    * @hapi/inert
    * @hapi/basic
    * @hapi/lab
    * @hapi/code
    * btoa
    * nodemon

* Second : run nodemon in your command line 
* Third : open http://localhost:8080/path
* Fourth : testing code
